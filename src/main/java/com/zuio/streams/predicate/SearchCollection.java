package com.zuio.streams.predicate;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SearchCollection {

    public static Predicate<A> searchName(String search) {
        return p -> p.getName().equalsIgnoreCase(search);
    }

    public static List<A> filter(List<A> a, Predicate<A> predicate) {
        return a.stream().filter(predicate).collect(Collectors.<A>toList());
    }

}
