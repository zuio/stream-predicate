package com.zuio.predicate;

import com.zuio.streams.predicate.A;
import com.zuio.streams.predicate.SearchCollection;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class SearchCollectionTest {

    @Test
    public void search() {
        List<A> list = createList(100);
        List<A> result = SearchCollection.filter(list, SearchCollection.searchName("A99"));
        Assert.assertEquals("A99", result.get(0).getName());
        result = SearchCollection.filter(list, SearchCollection.searchName("A100"));
        Assert.assertEquals(0, result.size());
    }

    public static List<A> createList(int size) {
        List<A> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            A a = new A();
            a.setId(i);
            a.setName("A" + i);
            list.add(a);
        }
        return list;
    }

}
